package auth.error.exceptions;

import auth.error.exceptions.base.BaseCustomException;

public class BadUserInputException extends BaseCustomException {

    public BadUserInputException(String message) {
        super(message, 400);
    }
}
