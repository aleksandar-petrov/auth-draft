package auth.error.exceptions;


import auth.error.exceptions.base.BaseCustomException;

public class NotFoundException extends BaseCustomException {

    public NotFoundException(String message) {
        super(message, 404);
    }
}
