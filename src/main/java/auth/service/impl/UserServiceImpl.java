package auth.service.impl;

import auth.dto.UserInfoResponseDto;
import auth.dto.UserRegisterRequestDto;
import auth.error.exceptions.BadUserInputException;
import auth.model.User;
import auth.repository.UserRepository;
import auth.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final ModelMapper modelMapper;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserServiceImpl(UserRepository userRepository, ModelMapper modelMapper, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> userCandidate = this.userRepository.findByUsername(username);

        return userCandidate.orElseThrow(() -> new UsernameNotFoundException(username));
    }

    @Override
    public UserInfoResponseDto registerUser(UserRegisterRequestDto userModel) {

        if (!userModel.getConfirm().equals(userModel.getPassword())) {
            throw new BadUserInputException("Please, enter matching passwords.");
        }

        if (this.userRepository.findByUsername(userModel.getUsername()).isPresent()) {
            throw new BadUserInputException("This username is already used.");
        }

        User user = this.modelMapper.map(userModel, User.class);
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        this.userRepository.save(user);

        return this.modelMapper.map(user, UserInfoResponseDto.class);
    }

    @Override
    public UserInfoResponseDto getUserInfoByUsername(String username) {
        Optional<User> userCandidate = this.userRepository.findByUsername(username);

        return this.modelMapper.map(
                userCandidate.orElseThrow(() -> new UsernameNotFoundException(username)),
                UserInfoResponseDto.class
        );
    }
}
