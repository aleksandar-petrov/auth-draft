package auth.service;


import auth.dto.UserInfoResponseDto;
import auth.dto.UserRegisterRequestDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    UserInfoResponseDto registerUser(UserRegisterRequestDto user);

    UserInfoResponseDto getUserInfoByUsername(String username);
}
