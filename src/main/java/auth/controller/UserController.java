package auth.controller;

import auth.dto.UserInfoResponseDto;
import auth.dto.UserRegisterRequestDto;
import auth.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("api")
public class UserController {

    private final UserService userService;

    private final ModelMapper modelMapper;

    public UserController(UserService userService, ModelMapper modelMapper) {
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    @PostMapping("/users")
    public ResponseEntity<UserInfoResponseDto> register(@RequestBody UserRegisterRequestDto user) {
        UserInfoResponseDto userInfoResponseDto = this.userService.registerUser(
                this.modelMapper.map(user, UserRegisterRequestDto.class));

        return ResponseEntity.status(HttpStatus.CREATED).body(userInfoResponseDto);
    }

    @GetMapping("/users/me")
    public ResponseEntity<UserInfoResponseDto> getProfileInfo(Principal principal) {
        return ResponseEntity.ok().body(this.userService.getUserInfoByUsername(principal.getName()));
    }
}
