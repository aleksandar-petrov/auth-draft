DROP DATABASE IF EXISTS auth;
CREATE DATABASE auth;

CREATE SCHEMA public
    AUTHORIZATION postgres;

DROP TABLE IF EXISTS public.users;
CREATE TABLE public.users
(
    username text NOT NULL,
    id text NOT NULL,
    password text NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE public.users
    OWNER to postgres;